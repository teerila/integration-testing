// TDD - unit testing

const expect = require("chai").expect;
const converter = require("../src/converter");

describe("Color Code Converter", () => {
    describe("RGB to HEX conversions", () => {
        it("converts the basic colors", () => {
            const redHex = converter.rgbToHex(255, 0, 0); // #ff0000
            expect(redHex).to.equal("#FF0000"); // red hex value

            const greenHex = converter.rgbToHex(0, 255, 0); // #00ff00
            expect(greenHex).to.equal("#00FF00"); // green hex value

            const blueHex = converter.rgbToHex(0, 0, 255); // #0000ff
            expect(blueHex).to.equal("#0000FF"); // blue hex value
        });
    });
    describe("HEX to RGB conversions", () => {
        it("converts the basic colors", () => {
            const redRgb = converter.hexToRgb("#ff0000"); // 255,0,0
            expect(redRgb).to.equal("RGB(255,0,0)"); // red RGB value

            const greenRgb = converter.hexToRgb("#00ff00"); // 0,255,0
            expect(greenRgb).to.equal("RGB(0,255,0)"); // green RGB value

            const blueRgb = converter.hexToRgb("#0000ff"); // 0,0,255
            expect(blueRgb).to.equal("RGB(0,0,255)"); // blue RGB value
        });        
    });
});