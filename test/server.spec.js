const expect = require("chai").expect;
const request = require("request");
const { response } = require("../src/server");
const app = require("../src/server");
const PORT = 3000;

describe("Color Code Converter API", () => {
    before("Starting server", (done) => {
        server = app.listen(PORT, () => {
            done();
        });
    });
    describe("RGB to Hex conversion", () => {
        const baseurl = `http://localhost:${PORT}`;
        const url = baseurl + "/rgb-to-hex?r=255&g=0&b=0";
        it("returns status 200", (done) => {
            request(url, (error, response, body) => {
                expect(response.statusCode).to.equal(200);
                done();
            });            
        });
        it("returns the color in hex", (done) => {
            request(url, (error, response, body) => {
                expect(body).to.equal("#FF0000");
                done();
            });
        });
    });
    describe("Hex to RGB conversion", () => {
        const baseurl = `http://localhost:${PORT}`;
        const url = baseurl + '/hex-to-rgb?h=0000ff';
        it("returns status 200", (done) => {
            request(url, (error,response, body) => {
                expect(response.statusCode).to.equal(200);
                done();
            });
        });
        it("returns the color in rgb", (done) => {
            request(url, (error, response, body) => {
                expect(body).to.equal("RGB(0,0,255)")
                done();
            });
        });
    });
    after("Stop server", (done) => {
        server.close();
        done();
    });
});