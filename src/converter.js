/**
 * Padding output to match 2 characters always.
 * @param {string} hex one or two characters
 * @returns {string} hex with two characters
 */
const pad = (hex) => {
    return (hex.length === 1 ? "0" + hex : hex);
};

module.exports = {
    /**
     * Converts the RGB values to a Hex string
     * @param {number} red 0-255
     * @param {number} green 0-255
     * @param {number} blue 0-255
     * @returns {string} hex value
     */
    rgbToHex: (red, green, blue) => {
        const redHex = red.toString(16);
        const greenHex = green.toString(16);
        const blueHex = blue.toString(16);
        const hex = "#" + pad(redHex) + pad(greenHex) + pad(blueHex);
        return hex.toUpperCase();
    },
    /**
     * Converts the hex values to RGB values
     * @param {string} hex #000000 - #ffffff
     * @returns {string} RGB value
     */
    hexToRgb: (hex) => {

        if (hex.charAt(0) === '#') {
            hex = hex.substring(1);
        }

        const r = parseInt(hex.substring(0,2), 16);
        const g = parseInt(hex.substring(2,4), 16);
        const b = parseInt(hex.substring(4,6), 16);
        
        const rgb = `RGB(${r},${g},${b})`;
        return rgb;
    }
}
